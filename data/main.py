from scipy.optimize import curve_fit
from scipy.stats import linregress
import matplotlib.pyplot as plt
import numpy as np
import math
import csv
import os
import matplotlib
from shapely.geometry import Polygon, LineString, Point
import tabulate
import sys

matplotlib.use('TKAgg')

data_node_1 = []
data_node_2 = []
data_node_3 = []

try:
    if str(sys.argv[1]) == 'lora':
        technology = 'lora'
        plot_rssi_title = 'Závislost RSSI na vzdálenosti pro LoRaWAN '
    elif str(sys.argv[1]) == 'zigbee':
        technology = 'zigbee'
        plot_rssi_title = 'Závislost RSSI na vzdálenosti pro ZigBee '
    else:
        print('Valid usage:')
        print('python3 ' + sys.argv[0] + ' [lora|zigbee]')
        print('\nTerminating script...')
        sys.exit(-1)
except IndexError as e:
    print('IndexError: {}'.format(e))
    print('Valid usage:')
    print('python3 ' + sys.argv[0] + ' [lora|zigbee]')
    print('\nTerminating script...')
    sys.exit(-1)

for [_, _, filenames] in os.walk(technology + '/'):
    for file in sorted(filenames):
        print(file)
        with open(os.path.join(technology, file), 'r', newline='') as csv_file:
            reader = csv.reader(csv_file, delimiter=';',
                                quoting=csv.QUOTE_NONNUMERIC)
            data_measurement_1 = []
            data_measurement_2 = []
            data_measurement_3 = []
            for row in reader:
                data_measurement_1.append(row[1])
                data_measurement_2.append(row[2])
                data_measurement_3.append(row[3])
        data_node_1.append(data_measurement_1)
        data_node_2.append(data_measurement_2)
        data_node_3.append(data_measurement_3)

data_node_1 = np.multiply(1, np.array(data_node_1))
data_node_2 = np.multiply(1, np.array(data_node_2))
data_node_3 = np.multiply(1, np.array(data_node_3))

print(data_node_1)

d_0 = 0.3 * math.sqrt(2)


def func_rssi_no_shadow(x, K, mu):
    """Return the RSSI with no long-term shadowing assumed (naive approach)

    Keyword arguments:
    x  -- distance in meters
    K  -- received power at the reference distance d0
    mu -- path-loss coeficient
    """
    return K - 10 * mu * np.log10(x / d_0)


def func_rssi_shadow(x, K, mu, X_sigma):
    """Return the RSSI with long-term shadowing assumed

    Keyword arguments:
    x  -- distance in meters
    K  -- received power at the reference distance d0
    mu -- path-loss coeficient
    X_sigma -- normal random variable with zero mean and standard deviation
    sigma
    """
    return K - 10 * mu * np.log10(x / d_0) + X_sigma


def func_d_shadow(RSSI, K, mu, X_sigma):
    """Return the distance from the RSSI with long-term shadowing assumed

    Keyword arguments:
    RSSI  -- RSSI at the distance of d
    K  -- received power at the reference distance d0
    mu -- path-loss coeficient
    X_sigma -- normal random variable with zero mean and standard deviation
    sigma
    """
    return 10 ** ((RSSI - K - X_sigma) / (-10 * mu)) * d_0


def func_MMSE_error(measured_data, model_data):
    """Return the minimal mean square error (see Goldsmith A., Wirelless
    Communications, c2004)

    Keyword arguments:
    measured_data -- data obtained from the measurement
    model_data -- data obtained via model
    """
    return np.sum(np.square((measured_data - model_data)))


distances = []
for i in range(4, 13):
    distances.append(i * 0.1 * math.sqrt(2) * np.ones(101))

distances_1 = []
distances_2 = []
distances_3 = []

distances = np.array(distances).flatten()
popt, pcov = curve_fit(func_rssi_no_shadow, distances.flatten(),
                       data_node_1.flatten())
model = func_rssi_no_shadow(distances, *popt)

error = func_MMSE_error(model, data_node_1.flatten())

sample_variance = error / len(distances)
print(sample_variance)

deviation = math.sqrt(sample_variance)

samples = np.random.normal(0, deviation, len(distances))

model_shadowing = func_rssi_shadow(distances, popt[0], popt[1], samples)
for i in range(0, 9):
    distances_1.append(np.average(
        func_d_shadow(
            data_node_1[i], popt[0], popt[1], np.random.normal(
                0, deviation, len(
                    data_node_1[i])))))
round_to_n = lambda x, n: x if x == 0 else round(x, -int(math.floor(math.log10(abs(x)))) + (n - 1))
plt_label = 'model RSSI, K = ' + str(round_to_n(popt[0], 4)) + 'dBm, μ = ' + str(round_to_n(popt[1], 4))

f_1 = plt.figure()
plt.plot(distances, model, 'r', label=plt_label)
plt.plot(distances, data_node_1.flatten(), 'b+', label='změřené hodnoty')
plt.ylabel('RSSI [dBm]')
plt.xlabel('d [m]')
plt.title(plot_rssi_title + '(uzel 1)')
plt.legend()
model = func_rssi_no_shadow(distances, *popt)

error = func_MMSE_error(model, data_node_2.flatten())

sample_variance = error / len(model)

deviation = math.sqrt(sample_variance)

samples = np.random.normal(0, deviation, len(distances))

model_shadowing = func_rssi_shadow(distances, popt[0], popt[1], samples)
for i in range(0, 9):
    distances_2.append(np.average(
        func_d_shadow(
            data_node_2[i], popt[0], popt[1], np.random.normal(
                0, deviation, len(
                    data_node_2[i])))))
model = func_rssi_no_shadow(distances, *popt)

error = func_MMSE_error(model, data_node_3.flatten())

sample_variance = error / len(model)

deviation = math.sqrt(sample_variance)

samples = np.random.normal(0, deviation, len(distances))

model_shadowing = func_rssi_shadow(distances, popt[0], popt[1], samples)
for i in range(0, 9):
    distances_3.append(np.average(
        func_d_shadow(
            data_node_3[i], popt[0], popt[1], np.random.normal(
                0, deviation, len(
                    data_node_3[i])))))

print(np.array(distances_1))
print(np.array(distances_2))
print(np.array(distances_3))

a_1, b_1 = (0, 0)
a_2, b_2 = (0, 1.65)
a_3, b_3 = (-2.4, 0)


def func_least_squares(x):
    A = np.array([[2 * (a_1 - a_3), 2 * (b_1 - b_3)], [2 * (a_2 - a_3), 2 * (b_2 - b_3)]])
    b = np.array([[a_1 ** 2 - a_3 ** 2 + b_1 ** 2 - b_3 ** 2 + x[2] ** 2 - x[0] ** 2],
                  [a_2 ** 2 - a_3 ** 2 + b_2 ** 2 - b_3 ** 2 + x[1] ** 2 - x[0] ** 2]])
    return np.linalg.inv(A.transpose() * A) * (A.transpose() * b)


def circle_intersection(circle1, circle2):
    '''
    calculates intersection points of two circles
    :param circle1: tuple(x,y,radius)
    :param circle2: tuple(x,y,radius)
    :return tuple of intersection ponts (which are (x,y) tuple)
    '''
    # return self.circle_intersection_sympy(circle1,circle2)
    x1, y1, r1 = circle1
    x2, y2, r2 = circle2
    # http://stackoverflow.com/a/3349134/798588
    dx, dy = x2 - x1, y2 - y1
    d = math.sqrt(dx * dx + dy * dy)
    if d > r1 + r2:
        print("#1")
        return '#1'  # no solutions, the circles are separate
    if d < abs(r1 - r2):
        print("#2")
        return '#2'  # no solutions because one circle is contained within the other
    if d == 0 and r1 == r2:
        print("#3")
        return '#3'  # circles are coincident and there are an infinite number of solutions

    a = (r1 * r1 - r2 * r2 + d * d) / (2 * d)
    h = math.sqrt(r1 * r1 - a * a)
    xm = x1 + a * dx / d
    ym = y1 + a * dy / d
    xs1 = xm + h * dy / d
    xs2 = xm - h * dy / d
    ys1 = ym - h * dx / d
    ys2 = ym + h * dx / d

    return (xs1, ys1), (xs2, ys2)


data_intersections_12 = []
data_intersections_23 = []
data_intersections_31 = []
measurements_12 = [None] * 9
measurements_23 = [None] * 9
measurements_31 = [None] * 9
full_intersection = []

for i in range(0, 9):
    tmp_12 = circle_intersection(
        (a_1, b_1, distances_1[i]), (a_2, b_2, distances_2[i]))
    tmp_23 = circle_intersection(
        (a_2, b_2, distances_2[i]), (a_3, b_3, distances_3[i]))
    tmp_31 = circle_intersection(
        (a_3, b_3, distances_3[i]), (a_1, b_1, distances_1[i]))
    if sorted(tmp_12) == sorted(tmp_23) == sorted(tmp_31):
        full_intersection.append([i, tmp_12])
    else:
        full_intersection.append(None)
        for j in range(0, 2):
            if tmp_12 not in ['#1', '#2', '#3'] and tmp_12[j][0] > 0:
                data_intersections_12.append([i, tmp_12[j]])
                measurements_12.insert(i, i)
            if tmp_23 not in ['#1', '#2', '#3'] and tmp_23[j][1] < tmp_23[j -
                                                                          1][1] and tmp_23[j][0] > tmp_23[j - 1][0]:
                data_intersections_23.append([i, tmp_23[j]])
                measurements_23.insert(i, i)
            if tmp_31 not in ['#1', '#2', '#3'] and tmp_31[j][1] > 0:
                data_intersections_31.append([i, tmp_31[j]])
                measurements_31.insert(i, i)

print(data_intersections_12)
print(data_intersections_23)
print(data_intersections_31)
print(full_intersection)

processed = []
P = []

unique_12 = []
unique_23 = []
unique_31 = []
partial_1223 = []
partial_2331 = []
partial_3112 = []

for i in range(0, 9):
    if measurements_12[i] != measurements_23[i] and measurements_12[i] != measurements_31[i] and measurements_12[
        i] is not None:
        unique_12.append(i)
    if measurements_23[i] != measurements_12[i] and measurements_23[i] != measurements_31[i] and measurements_23[
        i] is not None:
        unique_23.append(i)
    if measurements_31[i] != measurements_23[i] and measurements_31[i] != measurements_12[i] and measurements_31[
        i] is not None:
        unique_31.append(i)
    if measurements_12[i] == measurements_23[i] and measurements_12[i] != measurements_31[i] and measurements_12[
        i] is not None:
        partial_1223.append(i)
    if measurements_31[i] == measurements_23[i] and measurements_31[i] != measurements_12[i] and measurements_31[
        i] is not None:
        partial_2331.append(i)
    if measurements_12[i] != measurements_23[i] and measurements_12[i] == measurements_31[i] and measurements_12[
        i] is not None:
        partial_3112.append(i)

print(unique_12)
print(unique_23)
print(unique_31)
print(partial_1223)
print(partial_2331)
print(partial_3112)

plt.show()


def error_MSE(i, px, py):
    x = i * 0.1 + 0.4
    y = i * 0.1 + 0.4
    return np.sqrt((px - x) ** 2 + (py - y) ** 2)


def line_intersect(Ax1, Ay1, Ax2, Ay2, Bx1, By1, Bx2, By2):
    """ returns a (x, y) tuple or None if there is no intersection """
    d = (By2 - By1) * (Ax2 - Ax1) - (Bx2 - Bx1) * (Ay2 - Ay1)
    if d:
        uA = ((Bx2 - Bx1) * (Ay1 - By1) - (By2 - By1) * (Ax1 - Bx1)) / d
        uB = ((Ax2 - Ax1) * (Ay1 - By1) - (Ay2 - Ay1) * (Ax1 - Bx1)) / d
    else:
        return
    if not (0 <= uA <= 1 and 0 <= uB <= 1):
        return
    x = Ax1 + uA * (Ax2 - Ax1)
    y = Ay1 + uA * (Ay2 - Ay1)

    return x, y


def no_intersection(ri_1, ri_2, ri_3, cx_1, cx_2, cx_3, cy_1, cy_2, cy_3):
    '''
    :type ri_1: float
    :type ri_2: float
    :type ri_3: float
    :type cx_1: float
    :type cx_2: float
    :type cx_3: float
    :type cy_1: float
    :type cy_2: float
    :type cy_3: float
    :param ri_1: diameter of a smallests circle
    :param ri_2: diameter of a second smallest
    :param ri_3: diameter of the biggest circle
    :param cx_1: x-coordinate of the center of the smallest circle
    :param cx_2: x-coordinate of the center of a second smallest
    :param cx_3: x-coordinate of the center
    :param cy_1: y-coordinate of the center of the smallest circle
    :param cy_2: y-coordinate of the center of a second smallest
    :param cy_3: y-coordinate of the center
    :return: weights w_1 = r_1/r_2 and w_2 = r_1/r_3, d_1, d_2, x and y
    '''
    w_1 = ri_1 / ri_2
    w_2 = ri_1 / ri_3
    d = math.hypot(cx_1 - cx_2, cy_1 - cy_2)
    d = d - ri_1 - ri_2
    d_a = d * w_1
    p_1 = Point(cx_1, cy_1)
    p_2 = Point(cx_2, cy_2)
    l = LineString([p_1, p_2])
    c = p_1.buffer(ri_1, resolution=64)
    i = c.intersection(l)
    p_3 = (0, 0)
    for coo in range(0, 2):
        if i.coords[coo] != (p_1.x, p_1.y):
            p_3 = i.coords[coo]
            print(p_3)

    sl, _, _, _, _ = linregress([p_3[0], p_3[1]], [cx_3, cy_3])
    sl = math.atan(sl)
    d = math.hypot(p_3[0] - cx_3, p_3[1] - cy_3)
    d_b = math.sin(sl) * d * w_2
    x, y = line_intersect(a_2, b_2, a_1, b_1, p_3[0], p_3[1], a_3, b_3)

    return w_1, w_2, d_a, d_b, x, y


for i in full_intersection:
    if i is not None:
        if i[1] not in ['#1', '#2', '#3']:  # circles fully intersect in one point
            processed.append(i[0])
            r_1 = distances_1[i[0]]
            r_2 = distances_2[i[0]]
            r_3 = distances_3[i[0]]
            Ax = np.array([[(r_1 ** 2 - r_2 ** 2) - (a_1 ** 2 - a_2 ** 2) - (b_1 ** 2 - b_2 ** 2), 2 * (b_2 - b_1)],
                           [(r_1 ** 2 - r_3 ** 2) - (a_1 ** 2 - a_3 ** 2) - (b_1 ** 2 - b_3 ** 2), 2 * (b_3 - b_1)]])
            Bx = np.array([[2 * (a_2 - a_1), 2 * (b_2 - b_1)], [2 * (a_3 - a_1), 2 * (a_3 - a_1)]])
            Ay = np.array([[2 * (b_2 - b_1), (r_1 ** 2 - r_2 ** 2) - (a_1 ** 2 - a_2 ** 2) - (b_1 ** 2 - b_2 ** 2)],
                           [2 * (b_3 - b_1), (r_1 ** 2 - r_3 ** 2) - (a_1 ** 2 - a_3 ** 2) - (b_1 ** 2 - b_3 ** 2)]])
            By = np.array([[2 * (a_2 - a_1), 2 * (b_2 - b_1)], [2 * (a_3 - a_1), 2 * (a_3 - a_1)]])

            detAx = np.linalg.det(Ax)
            detBx = np.linalg.det(Bx)
            detAy = np.linalg.det(Ay)
            detBy = np.linalg.det(By)

            x = detAx / detBx
            y = detAy / detBy

            error_val = error_MSE(i[0], x, y)

            P.append([i[0], x, y, error_val])
        elif i[1] == '#1':  # circles do not intersect
            processed.append(i[0])
            d_1 = distances_1[i[0]]
            d_2 = distances_2[i[0]]
            d_3 = distances_3[i[0]]
            smallest = min([d_1, d_2, d_3])
            if smallest == d_1:
                if min([d_2, d_3]) == d_2:  # d_1 < d_2 < d_3
                    w_12, w_13, d_1a, d_2a, x, y = no_intersection(d_1, d_2, d_3, a_1, a_2, a_3, b_1, b_2, b_3)

                    error_val = error_MSE(i[0], x, y)

                    P.append([i[0], x, y, error_val])

                    print("d_1 = %f" % d_1a)
                    print("d_2 = %f" % d_2a)

                    print("w_12 = %f" % w_12)
                    print("w_13 = %f" % w_13)
                else:  # d_1 < d_3 < d_2
                    w_13, w_12, d_1a, d_2a, x, y = no_intersection(d_1, d_3, d_2, a_1, a_3, a_2, b_1, b_3, b_2)
                    error_val = error_MSE(i[0], x, y)

                    P.append([i[0], x, y, error_val])

                    print("d_1 = %f" % d_1a)
                    print("d_2 = %f" % d_2a)
                    print("w_13 = %f" % w_13)
                    print("w_12 = %f" % w_12)
            elif smallest == d_2:
                if min([d_1, d_3]) == d_1:  # d_2 < d_1 < d_3
                    w_21, w_23, d_1a, d_2a, x, y = no_intersection(d_2, d_1, d_3, a_2, a_1, a_3, b_2, b_1, b_3)
                    error_val = error_MSE(i[0], x, y)

                    P.append([i[0], x, y, error_val])

                    print("d_1 = %f" % d_1a)
                    print("d_2 = %f" % d_2a)
                    print("w_21 = %f" % w_21)
                    print("w_23 = %f" % w_23)
                else:  # d_2 < d_3 < d_1
                    w_23, w_21, d_1a, d_2a, x, y = no_intersection(d_2, d_3, d_1, a_2, a_3, a_1, b_2, b_3, b_1)
                    error_val = error_MSE(i[0], x, y)

                    P.append([i[0], x, y, error_val])

                    print("d_1 = %f" % d_1a)
                    print("d_2 = %f" % d_2a)
                    print("w_23 = %f" % w_23)
                    print("w_21 = %f" % w_21)
            else:
                if min([d_1, d_2]) == d_1:  # d_3 < d_1 < d_2
                    w_31, w_32, d_1a, d_2a, x, y = no_intersection(d_3, d_1, d_2, a_3, a_1, a_2, b_3, b_1, b_2)
                    error_val = error_MSE(i[0], x, y)

                    P.append([i[0], x, y, error_val])

                    print("d_1 = %f" % d_1a)
                    print("d_2 = %f" % d_2a)
                    print("w_31 = %f" % w_31)
                    print("w_32 = %f" % w_32)
                else:  # d_3 < d_2 < d_1
                    w_32, w_31, d_1a, d_2a, x, y = no_intersection(d_3, d_2, d_1, a_3, a_2, a_1, b_3, b_2, b_1)
                    error_val = error_MSE(i[0], x, y)

                    P.append([i[0], x, y, error_val])

                    print("d_1 = %f" % d_1a)
                    print("d_2 = %f" % d_2a)
                    print("w_32 = %f" % w_32)
                    print("w_31 = %f" % w_31)


def two_intersections(ri_1, ri_2, cx_1, cx_2, cx_3, cy_1, cy_2, cy_3):
    (x1, y1), (x2, y2) = circle_intersection((cx_1, cy_1, ri_1), (cx_2, cy_2, ri_2))

    d1 = math.hypot(cx_3 - x1, cy_3 - y1)
    d2 = math.hypot(cx_3 - x2, cy_3 - y2)

    if min([d1, d2]) == d1:
        return x1, y1
    else:
        return x2, y2


for m in range(0, 9):
    completed = False
    d_1 = distances_1[m]
    d_2 = distances_2[m]
    d_3 = distances_3[m]
    if m in unique_12:
        valx, valy = two_intersections(d_1, d_2, a_1, a_2, a_3, b_1, b_2, b_3)
        error_val = error_MSE(m, valx, valy)

        P.append([m, valx, valy, error_val])
    elif m in unique_23:
        valx, valy = two_intersections(d_2, d_3, a_2, a_3, a_1, b_2, b_3, b_1)
        error_val = error_MSE(m, valx, valy)

        P.append([m, valx, valy, error_val])
    elif m in unique_31:
        valx, valy = two_intersections(d_3, d_1, a_3, a_1, a_2, b_3, b_1, b_2)
        error_val = error_MSE(m, valx, valy)

        P.append([m, valx, valy, error_val])
    if m in partial_1223 or m in partial_2331 or m in partial_3112:
        matrix = func_least_squares((d_1, d_2, d_3))
        error_val = error_MSE(m, matrix[0][0], matrix[1][1])

        P.append([m, matrix[0][0], matrix[1][1], error_val])

    for i in data_intersections_12:
        for j in data_intersections_23:
            for k in data_intersections_31:
                d_1 = distances_1[i[0]]
                d_2 = distances_2[i[0]]
                d_3 = distances_3[i[0]]
                if i[0] == j[0] == k[0] == m:
                    completed = True
                    print(i[1])
                    print(j[1])
                    print(k[1])
                    centr = Polygon([i[1], j[1], k[1]]).centroid
                    print('x = {}, y = {}'.format(centr.x, centr.y))
                    dist_P12 = math.hypot(float(i[1][0]) - centr.x, float(i[1][1]) - centr.y)
                    dist_P23 = math.hypot(float(j[1][0]) - centr.x, float(j[1][1]) - centr.y)
                    dist_P31 = math.hypot(float(k[1][0]) - centr.x, float(k[1][1]) - centr.y)
                    smallest = min([d_1, d_2, d_3])
                    if smallest == d_1:
                        if min([d_2, d_3]) == d_2:  # d_1 < d_2 < d_3
                            w = d_1 / d_2
                            print("w_12 = %f" % w)
                        else:  # d_1 < d_3 < d_2
                            w = d_1 / d_3
                            print("w_13 = %f" % w)
                    elif smallest == d_2:
                        if min([d_1, d_3]) == d_1:  # d_2 < d_1 < d_3
                            w = d_2 / d_1
                            print("w_21 = %f" % w)
                        else:  # d_2 < d_3 < d_1
                            w = d_2 / d_3
                            print("w_23 = %f" % w)
                    else:
                        if min([d_1, d_2]) == d_1:  # d_3 < d_1 < d_2
                            w = d_3 / d_1
                            print("w_31 = %f" % w)
                        else:  # d_3 < d_2 < d_1
                            w = d_3 / d_2
                            print("w_32 = %f" % w)

                    dist = min(dist_P12, dist_P23, dist_P31)
                    if dist == dist_P12:
                        slope, _, _, _, _ = linregress([i[1], [centr.x, centr.y]])
                    elif dist == dist_P23:
                        slope, _, _, _, _ = linregress([j[1], [centr.x, centr.y]])
                    else:
                        slope, _, _, _, _ = linregress([k[1], [centr.x, centr.y]])
                    slope = math.atan(slope)
                    Px = centr.x + (1 - w) * dist * math.sin(slope)
                    Py = centr.y + (1 - w) * dist * math.cos(slope)

                    error_val = error_MSE(m, Px, Py)

                    P.append([m, Px, Py, error_val])

print(P)

with open(technology + '_table.tex', 'w') as ftex:
    ftex.write(tabulate.tabulate(P, headers=["měření", "x", "y", "chyba"], tablefmt='latex_booktabs'))

distance_x = []
distance_y = []
position_x = []
position_y = []
for i in range(0, 9):
    distance_x.append(i * 0.1 + 0.4)
    distance_y.append(-i * 0.1 - 0.4)
    position_x.append([P[i][1]])
    position_y.append([P[i][2]])

f, ax = plt.subplots(1)
plt.grid(which='both', axis='both')
plt.plot(distance_x, distance_y, 'r+', label='reálná pozice měřícího bodu')
plt.plot(position_x, position_y, 'b+', label='vypočtená pozice měřícího bodu')
plt.xlabel('souřadnice x [m]')
plt.ylabel('souřadnice y [m]')
for i in range(0, 9):
    d_x = distance_x[i]
    d_y = distance_y[i]
    p_x = position_x[i]
    print(p_x[0])
    p_y = position_y[i]
    label = f'#{i}'
    ax.annotate(label, xy=(float(d_x), float(d_y)), xytext=(d_x + 0.01, d_y + 0.01))
    label = f'#{i}'
    ax.annotate(label, xy=(p_x[0], p_y[0]), xytext=(p_x[0] + 0.01, p_y[0] + 0.01))
plt.legend()
plt.title('Vizualizace reálných a vypočtených pozic měřícího bodu')

f.savefig(technology + "distances.pdf", bbox_inches='tight')
f_1.savefig(technology + 'node_1.pdf', bbox_inches='tight')

plt.show()
