matplotlib~=3.3.0
numpy~=1.19.1
tabulate~=0.8.7
scipy~=1.5.2
Shapely~=1.7.0