import xbee
import time


## Infinite loop
#
# Infinite loop for sending message and its receive:
#
# 1. receive any message
# 2. if the message is not None (anything was received), parse message
# 3. If the message contains ACK, send SYN
#
# @return None
def loop():
    while True:
        msg = xbee.receive()
        msg_payload = None
        if msg is not None:
            msg_payload = msg['payload'].decode('utf-8')
            if msg_payload == 'ACK':
                print('received message: \n')
                print(msg)
                time.sleep(1)
                try:
                    xbee.transmit(xbee.ADDR_COORDINATOR, 'SYN')
                except Exception as e:
                    print("Transmit failure %s" % str(e))


## Setup the device with AT commands sent by xbee.atcmd:
# 
# @var name(ATNI) -- BASE_STATION_(number)
# @var power_level(ATPL) -- 4 (max, 8dBm)
# @var function(ATCE) -- router
# @var network ID(ATNI) -- 3223
# 
# @return True if connected
def setup():
    bs_number = input("Insert base station number")
    bs_name = 'BASE_STATION_' + str(bs_number)
    xbee.atcmd('NI', bs_name)
    xbee.atcmd('PL', 4)
    xbee.atcmd('WR')  # write the AT commands to memory
    xbee.atcmd('CE', 0)
    xbee.atcmd('WR')
    xbee.atcmd('ID', 3223)
    xbee.atcmd('WR')
    xbee.atcmd('AC')  # escape AT settings
    if wait_for_connect():
        return True


## Wait for the device to connect to a network
# 
# @return True if connected
def wait_for_connect():
    while xbee.atcmd('AI') != 0:
        print('Not connected')
        time.sleep_ms(100)

    print('Connected to network, 16-bit address: %s ' % str(xbee.atcmd('MY')))
    return True


if __name__ == '__main__':
    if setup():
        # send initial message
        while not xbee.receive():
            try:
                xbee.transmit(xbee.ADDR_COORDINATOR, 'SYN')
            except Exception as e:
                print("Transmit failure %s" % str(e))
        loop()
