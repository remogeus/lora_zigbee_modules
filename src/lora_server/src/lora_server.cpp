#include "ESP8266WiFi.h"
#include "Arduino.h"
#include "ESP8266mDNS.h"
#include "ESP8266WebServer.h"
#include "WiFiUdp.h"
#include "RH_RF95.h"
#include "SPI.h"
#include <math.h>
#include "ArduinoOTA.h"
#include "FS.h"

volatile bool interruptFlag = false;/**< sets up the interrupt flag for graceful reset - due to timing constraints, the code itself is run in loop() */

// definitions
#define RFM95_FREQ	868.0         /**< defines frequency of RFM95 to be 868MHz*/
#define RFM95_CS 	15            /**< defines SPI chip select pin on ESP8266 as GPIO15*/
#define RFM95_INT	5             /**< defines interrupt pin for RFM95 on ESP8266 as GPIO5 */
#define ESP_RELOAD	4             /**< defines pin for graceful shutdown of ESP8266 as GPIO4 */

#define SYNCH_LENGTH 5            /**< defines length of a synchronization packet */
#define ESP_LED 2

void handleInterrupt();
void setupDevices();
void startMDNS();
void startServer();
void startOTA();
void startSPIFFS();

void handleRoot();
void handleNotFound();

void sendStart(int RFM95_RXID);
int16_t receiveMessage();

String getContentType(String filename);
bool handleFileRead(String path);

void blinkLedSuccess(int LED_ID);
void blinkLedFailure(int LED_ID);

// global variables and instances
ESP8266WebServer server(80); /**< HTTP webserver on port 80 */
WiFiUDP UDP;                /**< instance of UDP */

const char* mdnsName = "esp8266lora";  /**< mDNS domain name */
const char* hostName = "esp8266lora";  /**< wifi hostname */
const char* wifiSSID = "HomeWifi";         /**< wifi SSID - include your own */
const char* wifiPass = "oa1CShqRD";         /**< wifi password - include your own */

const char* OTAName = "ESP8266";        /**< Name for OTA authetification */
const char* OTAPass = "esp8266";        /**< Password for OTA authentification */

unsigned long intervalSynch = 300;               /**< Between 2 measurements, there is a 5 minute delay */

RH_RF95 RFM95(RFM95_CS, RFM95_INT); /**< singleton instance of the radio driver */
RH_RF95::ModemConfigChoice configChoice = RH_RF95::ModemConfigChoice::Bw500Cr45Sf128; /**< Setup for RFM95 for values - Bw 500kHz, Cr 4/5, Sf 128 chips/symbol, CRC on */

const int RFM95_RXID1 = 1;                                      /**< ID of the first module */
const int RFM95_RXID2 = 2;                                      /**< ID of the second module */
const int RFM95_RXID3 = 3;                                      /**< ID of the third module */

                                             /**< Measurement counter */
unsigned long prevMeas = 0;                                     /**< time of previous measurement */

bool dropMeas = false;                                          /**< if we ever aquire incomplete measurement, it is automatically dropped */

int measCount = 0;

/**  Setup loop, run only on start
 *
 *
 */

void setup()
{
    Serial.begin(115200); // start the serial line for debug messages and programming, baud rate consistent for ESP8266
    delay(10);
    Serial.println('\n');

    pinMode(ESP_RELOAD, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(ESP_RELOAD), handleInterrupt, FALLING);

    setupDevices();

    startOTA();

    startMDNS();

    startSPIFFS();

    startServer();

    if(SPIFFS.exists("/data.csv")){
       SPIFFS.remove("/data.csv");
    }
}

/** @brief Infinite superloop
 *
 *  Infinite superloop, handling first the mDNS updates, then server, OTA and NTP handles and the measurements itself
 */


void loop()
{
    MDNS.update();
    while(measCount < 100){
        if(interruptFlag == true) {
            Serial.flush();
            WiFi.disconnect();
            ESP.restart();
        } else {
            ArduinoOTA.handle();
            server.handleClient();

            unsigned long currentMillis = millis();

            if(currentMillis - prevMeas > intervalSynch) {
                prevMeas = currentMillis;

                sendStart(RFM95_RXID1);
                int16_t rssi1 = receiveMessage();

                sendStart(RFM95_RXID2);
                int16_t rssi2 = receiveMessage();

                sendStart(RFM95_RXID3);
                int16_t rssi3 = receiveMessage();

                if(!dropMeas) {
                    Serial.printf("\nWriting measurement %d", measCount);
                    Serial.print("\n");
                    File file = SPIFFS.open("/data.csv", "a");
                    if (!file) {
                        Serial.println("Error opening file for writing");
                        return;
                    }
                    int bytesWritten = 0;
                    bytesWritten += file.print(measCount);
                    bytesWritten += file.print(";");
                    bytesWritten += file.print(rssi1);
                    bytesWritten += file.print(";");
                    bytesWritten += file.print(rssi2);
                    bytesWritten += file.print(";");
                    bytesWritten += file.print(rssi3);
                    bytesWritten += file.print("\n");
                    if (bytesWritten > 0) {
                        Serial.printf("\nFile was written, %d bytes", bytesWritten);

                    } else {
                        Serial.println("File write failed");
                    }
                    file.close();
                    Serial.println();
                    measCount++;
                }
            }
        }
    }
}

/** @brief Setup all of the devices
 *
 *	Setups all of the devices on board (ESP8266-12E and RFM95) and attaches
 *	interrupt for a soft reset button. This function is called once in
 *	setup() function after initialization.
 */
void setupDevices()
{
    //WIFI SETTINGS FOR ESP8266
    //
    WiFi.disconnect(); //disconnect, just in case
    WiFi.hostname(hostName);
    WiFi.begin(wifiSSID, wifiPass);

    Serial.print("Connecting...");
    // checks if we're connected to wifi
    while(WiFi.status() != WL_CONNECTED) {
        delay(1000);
        Serial.print('.');
    }

    // sets up static IP address
    //WiFi.config(ip,gateway,subnet);

    // debug messages, remmember to comment or delete
    Serial.println('\n');
    Serial.print("Connected to ");
    Serial.println(WiFi.SSID());
    Serial.print("IP address:\t");
    Serial.println(WiFi.localIP());

    // RFM95 SETTINGS
    while (!RFM95.init()) {
        Serial.println("LoRa radio init failed");
    }
    Serial.println("LoRa radio init OK");

    // defaults after init are 434.0MHz, modulation GFSK_Rb250Fd250, +13dBm
    // Bw = 125 kHz, Cr = 4/5, Sf = 128 chips/symbol and CRC on
    // To comply with CTU directive, frequency is set to ISM 868.0MHz,
    // power is set to 8 dBm (max power for Xbee3)
    // For faster communication, Bw is set to 500 kHz
    if(!RFM95.setFrequency(RFM95_FREQ)) {
        Serial.println("setFrequency failed");
        while (1); // hang the device
    }
    RFM95.setTxPower(8, false);
    RFM95.setModemConfig(configChoice);
}

/** @brief Start mDNS responder
 *
 *	Starts mDNS (multicast DNS) responder with the name defined by mdnsName
 *	variable, writes status on serial line.
 */
void startMDNS()
{
    if(MDNS.begin(mdnsName)) {
        Serial.print("MDNS working! Responder name: http://");
        Serial.print(mdnsName);
        Serial.print(".local");
        Serial.println();
    } else {
        Serial.println("MDNS not working!");
    }
}

/** @brief Start OTA service
 *
 *	Starts OTA service for a possibility of Over-the-air updates of firmware
 */
void startOTA()
{
    ArduinoOTA.setHostname(OTAName);
    ArduinoOTA.setPassword(OTAPass);

    ArduinoOTA.onStart([]() {
        Serial.println("OTA Start");
    });
    ArduinoOTA.onEnd([]() {
        Serial.println("\r\nOTA End");
    });
    ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
        Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
    });
    ArduinoOTA.onError([](ota_error_t error) {
        Serial.printf("Error[%u]: ", error);
        if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
        else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
        else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
        else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
        else if (error == OTA_END_ERROR) Serial.println("End Failed");
    });
    ArduinoOTA.begin();
    Serial.println("OTA ready\r\n");
}

/** @brief Start HTTP server
 *
 *	Starts HTTP server on port 80 and define handles for different inputs,
 *	writes status on serial line.
 */
void startServer()
{
    server.on("/", handleRoot);

    server.onNotFound([](){
        if (!handleFileRead(server.uri())){
                        handleNotFound();
        }
    });

    server.begin();
    Serial.println("HTTP server started");
    MDNS.addService("http", "tcp", 80);
}

/** @brief Sends a synchronisation packet
 *
 *	Sends a synchronisation packet to all of the LoRa beacons.
 * 	@param RFM95_RXID - ID of the LoRa beacon
 *
 */
void sendStart(int RFM95_RXID)
{
    char radioPacket[SYNCH_LENGTH];
    sprintf(radioPacket, "%dSYN",  RFM95_RXID);
    Serial.print("Sending ");
    Serial.println(radioPacket);
    delay(10);
    radioPacket[SYNCH_LENGTH - 1] = 0;
    RFM95.send((uint8_t *)radioPacket, SYNCH_LENGTH);
    Serial.println("Waiting for completion");
    delay(10);
    if(!RFM95.waitPacketSent()) {
        Serial.println("Sending failed");
        blinkLedFailure(ESP_LED);
    } else {
        Serial.println("Success, waiting for response!");
    }
}

/** @brief Receives a message
 *
 *	Receive a synchronisation packet from the LoRa beacons.
 */
int16_t receiveMessage()
{
    Serial.println("Waiting for message...");
    int i = 0;
    bool timeout = false;
    while(!RFM95.available()) {
        delay(1000);
        Serial.print('.');
        i++;
        if(i == 10) {
            timeout = true;
            break;
        }
    }
    if (!timeout){
        Serial.println();
        Serial.println("Received message, parsing!");
        uint8_t buffer[RH_RF95_MAX_MESSAGE_LEN];
        uint8_t len = sizeof(buffer);
        memset(buffer, '\0', sizeof(buffer));
        if(RFM95.recv(buffer, &len)) {
            Serial.print("Got: ");
            Serial.println((char*) buffer);
            if(buffer[1] == 'A' &&
               buffer[2] == 'C' &&
               buffer[3] == 'K') {
                blinkLedSuccess(ESP_LED);
                return RFM95.lastRssi();
            } else {
                Serial.println("Error while receiving message");
                blinkLedFailure(ESP_LED);
                dropMeas = true;
                return 0;
            }
        }
    } else {
        Serial.println("Error while receiving message - no message received");
        blinkLedFailure(ESP_LED);
        blinkLedFailure(ESP_LED);
        blinkLedFailure(ESP_LED);
        dropMeas = true;
    }
}

/** @brief handler for "server/"
 *
 *  Sends response 200 and a message "Hello world", if the page "/" is accesed
 */
void handleRoot()
{
    server.send(200, "text/plain", "Hello world!");
}

/** @brief handler for unexpected handle
 *
 *  Sends response 404 in case of the unimplemented access on site
 */
void handleNotFound()
{
    server.send(404, "text/plain", "404: not found");
}


/** @brief interrupt handler
 *
 *  Handles interrupt call to reload module
 */
void handleInterrupt()
{
    interruptFlag = true;
}

/** @brief SPIFFS enable
 *
 *  mounts SPIFFS (SPI flash file system)
 */

void startSPIFFS(){
    bool success = SPIFFS.begin();
    if (success) {
        Serial.println("File system mounted with success");
    } else {
        Serial.println("Error mounting the file system");
    }
}

/** @brief Get MIME type of a file
 *
 *  Gets a MIME type of the provided file for file handler
 *  @param filename - string with filename of the file
 *  @return string - MIME type
 */

String getContentType(String filename) { // convert the file extension to the MIME type
  if (filename.endsWith(".html")) return "text/html";
  else if (filename.endsWith(".css")) return "text/css";
  else if (filename.endsWith(".csv")) return "text/csv";
  else if (filename.endsWith(".js")) return "application/javascript";
  else if (filename.endsWith(".ico")) return "image/x-icon";
  else if(filename.endsWith(".gz")) return "application/x-gzip";
  return "text/plain";
}

/** @brief Handle reading of a file
 *
 *  Streams either a gz compressed version of a file, or the original file to a client
 *  @param path - path to a file
 *  @return bool - status of the file sending
 */

bool handleFileRead(String path){  // send the right file to the client (if it exists)
  Serial.println("handleFileRead: " + path);
  if(path.endsWith("/")) path += "index.html";           // If a folder is requested, send the index file
  String contentType = getContentType(path);             // Get the MIME type
  String pathWithGz = path + ".gz";
  if(SPIFFS.exists(pathWithGz) || SPIFFS.exists(path)){  // If the file exists, either as a compressed archive, or normal
    if(SPIFFS.exists(pathWithGz))                          // If there's a compressed version available
      path += ".gz";                                         // Use the compressed version
    File file = SPIFFS.open(path, "r");                    // Open the file
    size_t sent = server.streamFile(file, contentType);    // Send it to the client
    file.close();                                          // Close the file again
    Serial.println(String("\tSent file: ") + path);
    return true;
  }
  Serial.println(String("\tFile Not Found: ") + path);
  return false;                                          // If the file doesn't exist, return false
}

/** @brief blinks the in-built LED on success
 *
 */

void blinkLedSuccess(int LED_ID) {
    pinMode(LED_ID, OUTPUT);
    int i = 0;
    while(i < 2){
        digitalWrite(LED_ID, LOW);
        delay(100);
        digitalWrite(LED_ID, HIGH);
        if(i != 1){
            delay(100);
        }
        i++;
    }
}

/** @brief blinks the in-built LED on failure
 *
 */

void blinkLedFailure(int LED_ID) {
    pinMode(LED_ID, OUTPUT);
    int i = 0;
    while(i < 3){
        digitalWrite(LED_ID, LOW);
        delay(500);
        digitalWrite(LED_ID, HIGH);
        if(i != 2){
            delay(500);
        }
        i++;
    }
}
