import uio
import uos
import xbee
import gc

TARGET_NODE_ID_1 = "BASE_STATION_1"
TARGET_NODE_ID_2 = "BASE_STATION_2"
TARGET_NODE_ID_3 = "BASE_STATION_3"


## Find the router devices
#
# Search for the router devices in the network
# @param node_id  -- node identificator
# @return device ID if successful, if not, None
def find_device(node_id):
    for dev in xbee.discover():
        if dev['node_id'] == node_id:
            return dev
    return None



## Helper function for deleting files in flash
#
# A helper function called only from REPL to delete all CSV files in flash
def init_filesystem():
    for file in uos.listdir():
        if 'csv' in file:
            print('Removing file %s' % file)
            uos.remove(file)


## Construct the correct filename
# 
# Construct the filename to be saved, so that the distance of the node
# is parsed into the filename
# @return filename
def construct_filename():
    file_name = 'data'

    dist_x = input("Insert x distance: ")
    if dist_x is not None:
        file_name += '_'
        file_name += str(dist_x)
    dist_y = input("Insert y distance: ")
    if dist_y is not None:
        file_name += '_'
        file_name += str(dist_y)

    file_name += '.csv'
    print('Measuerements will be saved into a file %s' % file_name)

    return file_name


## Find the other devices and parse address
#  
#  Find the routers in the network and return their
#  16bit and 64bit addressess
#  @return 16 and 64bit address if succesful, if not, None
def find_and_parse_address(node_id):
    device = find_device(node_id)
    try:
        addr16 = device['sender_nwk']
        addr64 = device['sender_eui64']
        print('Found device {}'.format(node_id))
        return addr16, addr64
    except TypeError:
        return None

## Wrapper function for sending
#  
#  A wrapper function for sending ACK
#  messages to routers to pretify code
#  
# @param addr16 -- 16bit address 
# @param addr64 -- 64bit address 
# @param node_id -- id of the used node
def send_back(addr16, addr64, node_id):
    try:
        xbee.transmit(addr16 if addr16 else addr64, 'ACK')
        print('ACK send to node {}'.format(node_id))
    except Exception as e:
        print("Transmit failure: %s" % str(e))

## Main fuction
#  
#  Main function of the program - first, the filename for
#  CSV file used for measurement is constructed and routers in the
#  network are found, their 16bit and 64bit addresses are then parsed. 
#  CSV file is then opened for append and the module starts
#  receiving packets. If the packet is received (msg is not None),
#  payload and the transmitting router's address is parsed. If
#  the message is 'SYN', the module checks which router 
#  has sent the message and accordingly saves the measurement 
#  into CSV file. If the message is received from the node 3, it means
#  the last measurement from a set was received, so the message
#  counter is iterated. The loop repeats until the message counter is
#  higher than 100, then the last measurement was completed and 
#  module drops back into a REPL
#
#  The main function was separated so that it is callable from REPL after
#  every distance is measured and the reset of module is not needed
def main():
    data_file = construct_filename()

    msg_counter = 0

    while not find_and_parse_address(TARGET_NODE_ID_1):
        print('Device {} not found'.format(TARGET_NODE_ID_1))
    addr16_1, addr64_1 = find_and_parse_address(TARGET_NODE_ID_1)
    while not find_and_parse_address(TARGET_NODE_ID_2):
        print('Device {} not found'.format(TARGET_NODE_ID_2))
    addr16_2, addr64_2 = find_and_parse_address(TARGET_NODE_ID_2)
    while not find_and_parse_address(TARGET_NODE_ID_3):
        print('Device {} not found'.format(TARGET_NODE_ID_3))
    addr16_3, addr64_3 = find_and_parse_address(TARGET_NODE_ID_3)
    msg_1_received = False
    msg_2_received = False

    with uio.open(data_file, mode="a") as f:
        while msg_counter <= 100:
            msg = xbee.receive()
            if msg is not None:
                msg_payload = msg['payload'].decode('utf-8')
                msg_addr16 = msg['sender_nwk']
                msg_addr64 = msg['sender_eui64']
                if msg_payload == 'SYN':
                    if msg_addr16 is addr16_1 or msg_addr64 is addr64_1:
                        if not msg_1_received:
                            msg_1_received = True
                            print('measurement %.d / 100\n' % msg_counter)
                            print(
                                "Measurement saved, bytes used: %d" %
                                f.write(
                                    "%d;%d" %
                                    (msg_counter, xbee.atcmd('DB'))))
                            send_back(addr16_1, addr64_1, TARGET_NODE_ID_1)

                    elif msg_addr16 is addr16_2 or msg_addr64 is addr64_2:
                        if msg_1_received and not msg_2_received:
                            msg_2_received = True
                            print('measurement %.d / 100\n' % msg_counter)
                            print(
                                "Measurement saved, bytes used: %d" %
                                f.write(
                                    ";%d" %
                                    (xbee.atcmd('DB'))))
                            send_back(addr16_2, addr64_2, TARGET_NODE_ID_2)

                    elif msg_addr16 is addr16_3 or msg_addr64 is addr64_3:
                        if msg_1_received and msg_2_received:
                            print('measurement %.d / 100\n' % msg_counter)
                            print("Measurement saved, bytes used: %d" %
                                  f.write(";%d\n" % (xbee.atcmd('DB'))))
                            send_back(addr16_2, addr64_2, TARGET_NODE_ID_2)

                            msg_counter += 1
                            gc.collect()
                    else:
                        pass

    print('Measurement completed, dropping back to REPL')

## Setup the device with AT commands sent by xbee.atcmd:
# 
# @var name(ATNI) -- MOBILE_STATION
# @var power_level(ATPL) -- 4 (max, 8dBm)
# @var function(ATCE) -- coordinator
# @var network ID(ATNI) -- 3223
# 
# @return True 
def setup():
    xbee.atcmd('NI', 'MOBILE_STATION')
    xbee.atcmd('WR')
    xbee.atcmd('PL', 4)
    xbee.atcmd('WR')
    xbee.atcmd('CE', 1)
    xbee.atcmd('WR')
    xbee.atcmd('ID', 3223)
    xbee.atcmd('WR')
    xbee.atcmd('AC')
    return True

if __name__ == '__main__':
    if setup():
        main()
