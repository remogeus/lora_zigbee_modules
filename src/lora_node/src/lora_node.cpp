#include <SPI.h>
#include <RH_RF95.h>
#include <Arduino.h>
#include <stdlib.h>

// define used pins (for NSS and INT on RFM95 - using Arduino mappings)
#define RFM95_CS    10          /**< defines SPI chip select pin on ATMega328P-AU as Arduino D10 (PB2 - pin 14)*/
#define RFM95_INT   2           /**< defines interrupt pin for RFM95 on ATMega328P-AU as Arduino D2 (PD2 - pin 32) */
#define RFM95_FREQ  868.0       /**< defines frequency of RFM95 to be 868MHz*/

#define RFM95_ID	  3         /**< defines ID of a node */

RH_RF95 RFM95(RFM95_CS, RFM95_INT); /**< singleton instance of the radio driver */
RH_RF95::ModemConfigChoice configChoice = RH_RF95::ModemConfigChoice::Bw500Cr45Sf128;  /**< Setup for RFM95 for values - Bw 500kHz, Cr 4/5, Sf 128 chips/symbol, CRC on */

void setupDevice();

/**  Setup loop, run only on start
 *
 *
 */
void setup()
{
    // for debugging purposes
    Serial.begin(9600);

    setupDevice();
}

/** @brief Infinite superloop
 *
 *  Infinite superloop, handling the receiving and sending of packets
 */
void loop()
{
    if(RFM95.available()) { // if there's a message...
        uint8_t buffer[RH_RF95_MAX_MESSAGE_LEN];  // create a buffer for it
        uint8_t len = sizeof(buffer);
        char radioPacket[5];                    // for sending packet
        memset(buffer, '\0', sizeof(buffer));   // clean buffer
        if(RFM95.recv(buffer, &len)) {          // start the receiver and read the message
            if((int)buffer[0] - 48 == RFM95_ID) {   // if the message starts with ID of the module, it is for the module
                if(   buffer[1] == 'S'
                   && buffer[2] == 'Y'
                   && buffer[3] == 'N') { // check if the message contains SYN
                Serial.println("Received SYN, sending ACK");
                sprintf(radioPacket, "%dACK",  RFM95_ID);  // create a message nACK, where n is ID number
                radioPacket[4] = 0; // terminate message with NULL
                RFM95.send((uint8_t *)radioPacket, sizeof(radioPacket)); // send the message
                RFM95.waitPacketSent(); //wait for the message to be send
                } else { // in case of invalid message
                    Serial.println("ERROR_InvalidPacket");
                    sprintf(radioPacket, "%dERR",  RFM95_ID); // create a message nERR, where n is ID number
                    radioPacket[4] = 0;
                    RFM95.send((uint8_t *)radioPacket, sizeof(radioPacket));
                    RFM95.waitPacketSent();
                }
            }
        }
    }
}

/** @brief Setup all of the devices
 *
 *	Setups all of the devices on board (RFM95). This function is called once in
 *	setup() function after initialization.
 */

void setupDevice()
{
    // RFM95 SETTINGS
    while (!RFM95.init()) {
        Serial.println("LoRa radio init failed");
    }
    Serial.println("LoRa radio init OK");

    // defaults after init are 434.0MHz, modulation GFSK_Rb250Fd250, +13dBm
    // Bw = 125 kHz, Cr = 4/5, Sf = 128 chips/symbol and CRC on
    // To comply with CTU directive, frequency is set to ISM 868.0MHz,
    // power is set to 8 dBm (used Xbee module has a max Tx power of 8 dBm)
    // For faster communication, Bw is set to 500 kHz
    if(!RFM95.setFrequency(RFM95_FREQ)) {
        Serial.println("setFrequency failed");
        while (1); // hang the device
    }
    RFM95.setTxPower(8, false);
    RFM95.setModemConfig(configChoice);
}
