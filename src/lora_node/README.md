# LoRa node

Measurement node for the bachelor thesis

## Notes for compilation

This is a [PlatformIO](https://platformio.org/) project, therefore a [PlatformIO Core](https://platformio.org/install/cli) framework must be installed first. After that, the project can be initialized using

```
$ pio init
```

and then by running

```
$ pio run
```

PlatformIO automatically downloads pulls all of the required libraries and builds the project using the source code in src/ folder. After that, using

```
$ pio run -t upload
```

uploads firmware to a target. 
